package models

import javax.xml.bind.DatatypeConverter
import java.util.Calendar
import java.util.GregorianCalendar
import java.util.Date
import java.lang.StringBuilder
import com.google.gson.Gson
import java.text.SimpleDateFormat

object UserEvent {
  val ACTION_OPEN = "user-opened-app"
  val ACTION_VIEW_1 = "user-opened-view-1"
  val ACTION_VIEW_2 = "user-opened-view-2"
  val ACTION_VIEW_3 = "user-opened-view-3"
  val ACTION_CLOSE = "user-closed-app"
    
  def apply(userEvent: String) = {
    new Gson().fromJson(userEvent, classOf[UserEvent])
  }
}

class UserEvent extends Ordered[UserEvent] with Serializable {

  private var action: String = _
  private var ip: String = _
  private var country_code: String = _
  private var timestamp: String = _
  private var user_id: String = _

  private lazy val calenday = DatatypeConverter.parseDateTime(timestamp)
//  private lazy val millis = calenday.get
  private lazy val dateTime = calenday.getTime()
  private lazy val weekOfYear = calenday.get(Calendar.WEEK_OF_YEAR)
  private lazy val year: String = dateTime.getYear().toString
  private lazy val day: String = new StringBuilder().append(dateTime.getYear()).append(dateTime.getMonth()).append(dateTime.getDay()).toString()

  def getDateTime = dateTime
  def getWeekOfYear = weekOfYear
  def getYear = year
  def getDay = day
  def getAction = action
  def getUserId = user_id

  override def compare(e: UserEvent): Int = if(this.calenday.getTimeInMillis() > e.calenday.getTimeInMillis()) 1 else -1

  override def toString = "[action:%s, ip:%s, countryCode:%s, time:%s, year:%s, day:%s]"
    .format(action, ip, country_code, timestamp, year, day)
}
