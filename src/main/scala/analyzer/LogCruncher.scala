package analyzer

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import com.google.gson.Gson
import models.UserEvent
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import scala.collection.mutable.HashSet
import scala.collection.SortedMap
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.Path
import org.apache.spark.storage.StorageLevel

object LogCruncher {

  val STORAGE_LEVEL = StorageLevel.MEMORY_AND_DISK_SER

  val properties = Seq(
    ("spark.serializer", "org.apache.spark.serializer.KryoSerializer"),
    ("spark.default.parallelism", "10"),
    ("spark.executor.memory", "1g"))

  def main(args: Array[String]) {
    run(args)
  }

  def run(args: Array[String]) {
    if (args.length < 3) {
      System.err.println("Usage: totalUsers | userPerDay | view3Ratio | view23Ratio <master> <input>")
      System.exit(1)
    }

    //Properties needs to be set before instantiating Spark context
    properties.foreach(p => System.setProperty(p._1, p._2))

    val command = args(0)
    val master = args(1)
    val input = args(2)

    val startTime = System.currentTimeMillis()

    val conf = new SparkConf()
      .setMaster(master)
      .setAppName("LogCruncher")
      .setJars(SparkContext.jarOfClass(this.getClass).toSeq)

    lazy val sc = new SparkContext(conf)

    lazy val logData = sc.textFile(input).persist(STORAGE_LEVEL)

    val output = command match {
      case "totalUsers" => uniqueUsersTotal(logData).toString
      case "userPerDay" => uniqueUsersPerDay(logData).map(e => e._1 + " : " + e._2).sortWith(_ < _).mkString("\n")
      case "view3Ratio" => view3ToOpenRatio(logData).toString
      case "view23Ratio" => view3ToOpenRatio(logData).toString
      case _ => throw new IllegalArgumentException("Unknown command")
    }

    sc.stop

    println("Result is " + output)

    println("Total time in millis " + (System.currentTimeMillis - startTime))
  }

  /**
   * Number of unique users per day use the application partition results by day.
   *
   * Returns an Arrays of tuples (date, number of unique visitors)
   * date is in format of yMMdd and y would be the year in 2000.
   */
  def uniqueUsersPerDay(logData: => RDD[String]): Array[(String, Long)] = {

    /**
     * Group all userIds by date after mapping each event to (date, userId)
     */
    val usersDay = logData.map(e => { val event = UserEvent(e); (event.getDay, event.getUserId) }).persist(STORAGE_LEVEL)

    val usersByDay = usersDay.groupByKey().persist(STORAGE_LEVEL)

    /**
     * We assumes all userIDs can easily fit in memory otherwise Bloom filter can be considered as an alternative
     * for HashSet here to avoid running out of memory for a very large data set of users.
     */
    val usersNumberPerDay = usersByDay.map(kv => {
      val userSet = HashSet[String]()
      var counter: Long = 0
      kv._2.foreach(elem => if (userSet.add(elem)) counter += 1)
      (kv._1, counter)
    }).persist(STORAGE_LEVEL)

    usersNumberPerDay.collect
  }

  /**
   * Number of unique users used the application during entire period
   */
  def uniqueUsersTotal(logData: => RDD[String]): Long = {
    val userIDs = logData.map(e => {
      val event = UserEvent(e)
      event.getUserId
    }).persist(STORAGE_LEVEL)

    userIDs.distinct.count
  }

  /**
   * The conversion percentage from user-­opened-­app to user­-opened-­view-­3
   * regardless of the path for the entire period.
   */
  def view3ToOpenRatio(logData: => RDD[String]): Float = {
    //Group events by users
    val eventsByUser = logData.map(e => {
      val event = UserEvent(e)
      (event.getUserId, event)
    }).groupByKey.persist(STORAGE_LEVEL)

    val countsPerUser = eventsByUser.map(idEvent => {
      //Sorting a single user in ascending order.
      //Sorting is performed in memory and we assume all events of 
      //a single user easily fits in memory (at most requires few MBs).
      val sortedEvents = idEvent._2.sortWith(_ < _)
      var openCount: Long = 0
      var openToV3Count: Long = 0
      var hasOpened = false
      sortedEvents.foreach(e => {
        e.getAction match {
          case UserEvent.ACTION_OPEN => {
            openCount += 1
            hasOpened = true
          }
          case UserEvent.ACTION_CLOSE => {
            hasOpened = false
          }
          case UserEvent.ACTION_VIEW_3 => {
            if (hasOpened) {
              openToV3Count += 1
              hasOpened = false
            }
          }
          case _ => { /*do nothing*/ }
        }
      })
      (openCount, openToV3Count)
    }).persist(STORAGE_LEVEL)

    //Aggregate opened actions counts as well as view3 via open counts
    val (opens, view3) = countsPerUser.reduce((a, b) => (a._1 + b._1, a._2 + b._2))

    view3.asInstanceOf[Float] / opens
  }

  /**
   * The conversion percentage from user­-opened­-app to user-­opened­-view­-3 via user­-opened­-view-2
   * regardless of the path for the entire period.
   */
  def view3ViaView2ToOpenRatio(logData: => RDD[String]): Float = {
    //Group events by user
    val eventsByUser = logData.map(e => {
      val event = UserEvent(e)
      (event.getUserId, event)
    }).groupByKey.persist(STORAGE_LEVEL)

    val countsPerUser = eventsByUser.map(idEvent => {
      //Sorting a single user in ascending order.
      //Sorting is performed in memory and we assume all events of 
      //a single user easily fits in memory (at most requires few MBs).
      val sortedEvents = idEvent._2.sortWith(_ < _)
      var openCount: Long = 0
      var v3ViaV2Count: Long = 0
      var hasOpened = false
      var hasViewd2 = false
      sortedEvents.foreach(e => {
        e.getAction match {
          case UserEvent.ACTION_OPEN => {
            openCount += 1
            hasOpened = true
            hasViewd2 = false
          }
          case UserEvent.ACTION_CLOSE => {
            hasOpened = false
            hasViewd2 = false
          }
          case UserEvent.ACTION_VIEW_2 => {
            if (hasOpened)
              hasViewd2 = true
          }
          case UserEvent.ACTION_VIEW_3 => {
            if (hasViewd2 && hasOpened) {
              v3ViaV2Count += 1
            }
            hasViewd2 = false
            hasOpened = false
          }
          case _ => { /*do nothing*/ }
        }
      })
      (openCount, v3ViaV2Count)
    }).persist(STORAGE_LEVEL)

    //Aggregate open counts as well as view3 via view 2 counts
    val (opens, view3) = countsPerUser.reduce((a, b) => (a._1 + b._1, a._2 + b._2))

    view3.asInstanceOf[Float] / opens
  }
}
