package serializer;

import com.esotericsoftware.kryo.Kryo
import org.apache.spark.serializer.KryoRegistrator;
import models.UserEvent

class SerializerRegister extends KryoRegistrator {
  override def registerClasses(kryo: Kryo) {
    kryo.register(classOf[String])
    kryo.register(classOf[UserEvent])
  }
}