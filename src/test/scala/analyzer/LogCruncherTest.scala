package analyzer

import org.apache.spark.SparkContext
import org.scalatest.FunSuite
import scala.collection.JavaConversions._
import org.junit.Assert._

class LogCruncherTest extends FunSuite {

  val sc = new SparkContext("local", "", "", SparkContext.jarOfClass(classOf[LogCruncherTest]))

  val sample1 = Seq(
    """{"action":"user-opened-app","ip":"115.36.177.28","user_id":"1","country_code":"PE","timestamp":"2001-01-01T00:00:00"}""",
    """{"action":"user-opened-app","ip":"251.216.230.156","user_id":"1","country_code":"PE","timestamp":"2001-01-01T00:00:00.100000"}""",
    """{"action":"user-opened-app","ip":"215.203.15.106","user_id":"2","country_code":"PE","timestamp":"2001-01-01T00:00:00.200000"}""",
    """{"action":"user-opened-app","ip":"215.203.15.106","user_id":"1","country_code":"PE","timestamp":"2001-01-02T00:00:00.300000"}""",
    """{"action":"user-opened-app","ip":"2.210.83.5","user_id":"2","country_code":"PE","timestamp":"2001-01-02T00:00:00.400000"}""",
    """{"action":"user-opened-app","ip":"2.210.83.5","user_id":"3","country_code":"PE","timestamp":"2001-01-02T00:00:00.500000"}""")

  val sample2 = Seq(
    """{"action":"user-opened-app","ip":"251.216.230.156","user_id":"1","country_code":"PE","timestamp":"2001-01-01T00:00:00"}""",
    """{"action":"user-closed-app","ip":"251.216.230.156","user_id":"1","country_code":"PE","timestamp":"2001-01-01T00:00:00.100000"}""",
    """{"action":"user-opened-app","ip":"251.216.230.156","user_id":"2","country_code":"PE","timestamp":"2001-01-01T00:00:00.20000"}""",
    """{"action":"user-opened-view-2","ip":"115.36.177.28","user_id":"1","country_code":"PE","timestamp":"2001-01-01T00:00:00.300000"}""",
    """{"action":"user-opened-view-3","ip":"115.36.177.28","user_id":"1","country_code":"PE","timestamp":"2001-01-01T00:00:00.400000"}""",
    """{"action":"user-opened-app","ip":"251.216.230.156","user_id":"1","country_code":"PE","timestamp":"2001-01-01T00:00:00.500000"}""")

  val sample3 = Seq(
    """{"action":"user-opened-app","ip":"251.216.230.156","user_id":"1","country_code":"PE","timestamp":"2001-01-01T00:00:00.100000"}""",
    """{"action":"user-opened-view-2","ip":"115.36.177.28","user_id":"1","country_code":"PE","timestamp":"2001-01-01T00:00:00.200000"}""",
    """{"action":"user-opened-view-3","ip":"115.36.177.28","user_id":"1","country_code":"PE","timestamp":"2001-01-01T00:00:00.300000"}""",
    """{"action":"user-closed-app","ip":"115.36.177.28","user_id":"1","country_code":"PE","timestamp":"2001-01-01T00:00:00.400000"}""",
    """{"action":"user-opened-app","ip":"115.36.177.28","user_id":"1","country_code":"PE","timestamp":"2001-01-01T00:00:00.500000"}""",
    """{"action":"user-opened-view-1","ip":"115.36.177.28","user_id":"1","country_code":"PE","timestamp":"2001-01-01T00:00:00.600000"}""",
    """{"action":"user-opened-view-3","ip":"115.36.177.28","user_id":"1","country_code":"PE","timestamp":"2001-01-01T00:00:00.700000"}""",
    """{"action":"user-opened-view-3","ip":"115.36.177.28","user_id":"1","country_code":"PE","timestamp":"2001-01-01T00:00:00.800001"}""",
    """{"action":"user-closed-app","ip":"115.36.177.28","user_id":"1","country_code":"PE","timestamp":"2001-01-01T00:00:00.900000"}""")

  test("uniqueUsersPerDay") {
    val logData = sc.parallelize(sample1, 3)
    val expectedResult = Array(("10101", 2), ("10102", 3))
    val actualResult = LogCruncher.uniqueUsersPerDay(logData).toMap
    assertEquals(2, actualResult("10101"))
    assertEquals(3, actualResult("10102"))
  }

  test("uniqueUsersTotal") {
    val logData = sc.parallelize(sample1, 3)
    val actualResult = LogCruncher.uniqueUsersTotal(logData)
    assertEquals(3, actualResult)
  }

    test("view3ToOpenRatio") {
      val logData1 = sc.parallelize(sample2, 3)
      val actualResult1 = LogCruncher.view3ToOpenRatio(logData1)
      assertEqualsFloat(0f, actualResult1, "views must be counted only after open and before close actions")
  
      val logData2 = sc.parallelize(sample3, 3)
      val actualResult2 = LogCruncher.view3ToOpenRatio(logData2)
      println(actualResult2)
      assertEqualsFloat(1f, actualResult2, "several identical views3 must not be counted within same visit")
    }

  test("view3ViaView2ToOpenRatio") {
    val logData1 = sc.parallelize(sample2, 3)
    val actualResult1 = LogCruncher.view3ViaView2ToOpenRatio(logData1)
    println(actualResult1)
    assertEqualsFloat(0f, actualResult1, "view must be counted only after open and before close actions")

    val logData2 = sc.parallelize(sample3, 3)
    val actualResult2 = LogCruncher.view3ViaView2ToOpenRatio(logData2)
    println(actualResult2)
    assertEqualsFloat(0.5f, actualResult2, "several identical views must not be counted within same visit")
  }

  def assertEqualsFloat(a: Float, b: Float, msg: String) {
    assertTrue(msg, (a - b).abs < 0.001)
  }
}