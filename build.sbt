import AssemblyKeys._
import com.twitter.sbt._

name := "Event-Cruncher"

version := "1.0-SNAPSHOT"

scalaVersion := "2.10.0"

javaOptions in run += "-Xmx4G"

resolvers += "Akka repository" at "http://repo.akka.io/releases"

resolvers += "ClouderaRepo" at "https://repository.cloudera.com/content/repositories/releases"

ivyXML :=
<dependency org="org.eclipse.jetty.orbit" name="javax.servlet" rev="2.5.0.v201103041518">
<artifact name="javax.servlet" type="orbit" ext="jar"/>
</dependency>

libraryDependencies ++= Seq(
"org.apache.spark" % "spark-core_2.10" % "0.9.0-incubating",
"org.scalatest" % "scalatest_2.10" % "2.0" % "test",
"junit" % "junit" % "4.4",
"com.google.code.gson" % "gson" % "2.2.4",
"com.novocode" % "junit-interface" % "0.9" % "test",
"org.apache.hadoop" % "hadoop-client" % "2.0.0-mr1-cdh4.0.1")

parallelExecution := false

javaOptions += "-XX:+UseCompressedOops"

jarName in assembly := "cuncher-1_0.jar"

mainClass in assembly := Some("analyzer.Crucher")
